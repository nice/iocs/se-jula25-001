# ---
require essioc
require julabof25hl

epicsEnvSet(IPADDR, "172.30.32.24")
epicsEnvSet(IPPORT, "4001")
epicsEnvSet(SYSTEM, "SE-SEE")
epicsEnvSet(DEVICE, "SE-JULA25-001")
epicsEnvSet(PREFIX, "$(SYSTEM):$(DEVICE)")
epicsEnvSet(PORTNAME, "$(PREFIX)")
epicsEnvSet(TEMPSCAN, "1")
epicsEnvSet(CONFSCAN, "10")
epicsEnvSet(LOCATION, "$(SYSTEM); $(IPADDR)")
epicsEnvSet(STREAM_PROTOCOL_PATH, "$(julabof25hl_DIR)db")

drvAsynIPPortConfigure("$(PORTNAME)", "$(IPADDR):$(IPPORT)")

iocshLoad("$(essioc_DIR)/common_config.iocsh")
iocshLoad("$(julabof25hl_DIR)julabof25hl.iocsh", "P=$(PREFIX), R=:, PORT=$(PORTNAME), ADDR=$(IPPORT), TEMPSCAN=$(TEMPSCAN), CONFSCAN=$(CONFSCAN)")
# ...
